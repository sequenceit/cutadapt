# 9 July 2019

* `-g` flag added to the cutadapt script; I think this has been done correctly, though the pipeline treats
  paired-end files as individual files...
* `-j` flag (cores) incorporated as a hidden option on the Albiorix Sequenceit interface
  * When using `-j 0`, the program is supposed to detect the number of cores available; however, even if
    the queue command states that 4 cores are available, cutadapt still only uses a single core...

# 10 July 2019

Once a cutadapt job has completed on Albiorix, the incorrect files are displayed
* The original files are listed rather than the expected `.CA.fq.gz` files
  * Why?
  * Looking at file sizes, it seems that the expected files *are* being displayed, but with the original names!

# 22 July 2019

Uploaded version 0.0.8
* Updated Cutadapt from v1.16 > v2.4
  * This allows for use of the `-j 0` option for auto-detecting cores
* Added pigz installation to the image to improve speed
  * In the initial tests, this sped up processing of two samples by around 3x

# 31 July 2019
Retagged the most recent version:
* v0.0.8 -> v0.1.0

# 26 November 2019
Due to some confusion with where adapters appear in the sequences, the following versions have been pushed to Docker Hub:
* v0.1.0 - uses `-a` and `-g` flags
* v0.1.1 - uses `-a` and `-a` flags
* v0.1.2 - uses `-g` and `-g` flags

# 27 November 2019
Added code to try and get Cutadapt to run in paired end mode
* Flags must now be explicitly given
* May also need to refine the code so that if, for example, `_R1_002` and `_R2_002` are present,
  files aren't incorrectly paired

# 4 December 2019
Users can now specify their own flags for forward and reverse read adapters
* Through the Docker image, this works by wrapping the arguments in "quotes"
* Through SequenceIt, this works by wrapping the arguments in \"backslashes and quotes\"

This version implemented as v0.2.0

##########


## Testing command
```
docker run -it --volume `pwd`/input:/media/input --volume `pwd`/output:/media/output sequenceit/cutadapt:v0.2.0  --input /media/input/infile1 /media/input/infile2 [...] --output /media/output/ --fwd_adapter "-a AGATCGGAAGAGC" --rev_adapter "-A GCTCTTCCGATCT" --error_rate 0.1 --overlap 3 --minimum_length 50 --cores 4
```

## Interactive testing command
```
docker run -it --entrypoint /bin/bash --volume `pwd`/input:/media/input --volume `pwd`/output:/media/output sequenceit/cutadapt:v0.2.0
```
