FROM ubuntu:20.04

# Env
ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=Europe/Stockholm

# Defaults
RUN apt-get update
RUN apt-get install -y apt-utils
RUN apt-get install -y git
RUN apt-get install -y curl
RUN apt-get install -y libcurl4-gnutls-dev
RUN apt-get install -y libssl-dev
RUN apt-get install -y libcurl4-openssl-dev
RUN apt-get install -y libxml2-dev
RUN apt-get install -y python3-pip
RUN apt-get install -y pigz

# WD
RUN mkdir -p /opt/cutadapt
WORKDIR /opt/cutadapt

# Dependencies
RUN pip3 install cython
# The next line is redundant and this can be solved with a submodule instead.
# RUN git clone https://gitlab.com/sequenceit/pairedend.git /opt/cutadapt/sequenceit_utility

# Install cutadapt and pigz
RUN git clone --single-branch -b v2.10 https://github.com/marcelm/cutadapt.git
WORKDIR /opt/cutadapt/cutadapt
RUN python3 setup.py install


# Wrapper
COPY run_cutadapt.py /opt/cutadapt
COPY sequenceit_utility/ /opt/cutadapt/sequenceit_utility/

# Mountpoints
RUN mkdir -p /media/input /media/output

WORKDIR /opt/cutadapt
ENTRYPOINT ["python3", "/opt/cutadapt/run_cutadapt.py"]
CMD ["--help"]
