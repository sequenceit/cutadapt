This repository creates a docker image that can be used to run cutadapt analyses on fastq files.

# Dependencies
* Docker

# Download from Dockerhub
You can download the docker image from docker hub if you want to run this analysis locally.
```
docker pull sequenceit/cutadapt:v0.2.0
```

# Build from source
You can also build from source if you want to make changes to the docker image.
```
git clone git@gitlab.com:sequenceit/cutadapt.git
cd cutadapt
docker build --tag cutadapt:dev .
```

# Using the docker image
The docker image expects to find the fastq input files to analyse in the directory `input` in this repository. The result of the analysis will end up in the `output` directory.

```
docker run --volume `pwd`/input:/media/input  --volume `pwd`/output:/media/output cutadapt:dev --input-files /media/input/infile1 /media/input/infile2 [...] --output /media/output/ --fwd_adapter "-a AGATCGGAAGAGC" --rev_adapter "-A GCTCTTCCGATCT" --error_rate 0.1 --overlap 3 --minimum_length 50 --cores 4
```
or relay on default values
```
docker run --volume `pwd`/input:/media/input  --volume `pwd`/output:/media/output cutadapt:dev --input-files /media/input/* --output /media/output 
```

# Interactive session
```
docker run -it --entrypoint /bin/bash --volume `pwd`/input:/media/input  --volume `pwd`/output:/media/output cutadapt:dev
```

# Build and push image to Google cloud
```
docker build --tag=sequenceit/cutadapt:v2.10.0 .
docker tag sequenceit/cutadapt:v2.10.0 eu.gcr.io/sequenceit-207107/cutadapt:v2.10.0
gcloud docker -- push eu.gcr.io/sequenceit-207107/cutadapt:v2.10.0
```
