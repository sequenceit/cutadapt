#!/usr/bin/env python3

# Licence:

# Usage: run_cutadapt.py [options] -i <FILE_NAME> -o <FILE_NAME>

import sys
import argparse
import subprocess
from subprocess import Popen, PIPE
import logging as log
from sequenceit_utility.files import *


parser = argparse.ArgumentParser(
    prog=sys.argv[0],
    description="Run a cutadapt analysis",
    formatter_class=argparse.ArgumentDefaultsHelpFormatter,
)

parser.add_argument("-v", "--verbose", action="store_true", help="Be more verbose.")
parser.add_argument(
    "-i", "--input-files", nargs="*", help="One or several fastq files.", required=True
)
parser.add_argument("-o", "--output", help="Name of the output directory.", required=True)
parser.add_argument(
    "-j",
    "--cores",
    type=int,
    default=1,  # Auto-detect number of cores
    help="Number of CPU cores to use.",
)
parser.add_argument(
    "-a",
    "--fwd_adapter_3",
    type=str,
    default="AGATCGGAAGAGCACACGTCTGAACTCCAGTCA",
    help="Adapter sequence to remove from 3' end of first read of paired data.",
)
parser.add_argument(
    "-A",
    "--rev_adapter_3",
    type=str,
    default="AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGT",
    help="Adapter sequence to remove from 3' end of second read of paired data.",
)
parser.add_argument(
    "-g",
    "--fwd_adapter_5",
    type=str,
    default="AGATCGGAAGAGCACACGTCTGAACTCCAGTCA",
    help="Adapter sequence to remove from 5' end of first read of paired data.",
)
parser.add_argument(
    "-G",
    "--rev_adapter_5",
    type=str,
    default="AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGT",
    help="Adapter sequence to remove from 5' end of second read of paired data.",
)
parser.add_argument(
    "-e",
    "--error_rate",
    type=float,
    default=0.1,
    help="""Maximum allowed error rate (no. of errors divided by
                the length of the matching region).""",
)
parser.add_argument(
    "-O",
    "--overlap",
    type=int,
    default=3,
    help="""If the overlap between the read and the adapter is
                shorter than MINLENGTH, the read is not modified.
                Reduces the no. of bases trimmed due to random adapter
                matches.""",
)
parser.add_argument(
    "-m",
    "--minimum_length",
    type=int,
    default=50,
    help="""Discard reads shorter than LEN.""",
)
parser.add_argument(
    "-q",
    "--quality_cutoff",
    type=str,
    default="1,1",
    help="""Trim low-quality bases from 5' and/or 3' ends of each
                read before adapter removal. Applied to both reads if
                data is paired. If one value is given, only the 3' end
                is trimmed. If two comma-separated cutoffs are given,
                the 5' end is trimmed with the first cutoff, the 3'
                end with the second.""",
)
parser.add_argument(
    "-n",
    "--times",
    type=int,
    default=5,
    help="""Remove up to TIMES adapters from each read.""",
)

args = parser.parse_args()


def main():
    # Initialise log file
    logfile = str(args.output) + "/analysis.log"
    log.basicConfig(filename=logfile, format="%(asctime)s %(message)s", level=log.DEBUG)
    log.info("Analysis started.")

    # Store each input file as an InputFile object.
    input_files = []
    for input_file in args.input_files:
        input_files.append(InputFile(input_file))

    # Parse the input file(s) and analyse them one at the time.
#    print(input_files)
#    for input_files in paires(args.input_files, hashed=True):
    for file_pair in paires(input_files):
#        print(file_pair)
        try:

            files = in_out_files(file_pair, args.output, ".CA")
            ##################################
            # Put this stuff in a function and call it instead

            #            name_base, ext = no_file_ext(input_file)
            #            filename = name_base.split("/")[-1]
            #            out_file = args.output + "/" + filename + ".CA" + ext

            ##################################
            process_args = [
                "cutadapt",
                "-j",
                str(args.cores),
                "-a",
                args.fwd_adapter_3,
                "-A",
                args.rev_adapter_3,
                "-g",
                args.fwd_adapter_5,
                "-G",
                args.rev_adapter_5,
                "-e",
                str(args.error_rate),
                "-O",
                str(args.overlap),
                "-m",
                str(args.minimum_length),
                "-q",
                str(args.quality_cutoff),
                "-n",
                str(args.times),
                "-o",
                files["output1"],
                "-p",
                files["output2"],
                files["input1"],
                files["input2"],
            ]
            log.info(process_args)
            process = Popen(process_args, stdout=PIPE, stderr=PIPE)
            (stdout, stderr) = process.communicate()
            if stdout:
                log.info(f"Popen stdout: {stdout.decode()}")
            if stderr:
                log.error(f"Popen stderr: {stderr}")
#        except UnknownFileExtension:
#            # If file extentions is neither of .fastq, fastq.gz, .fq or .fq.gz.
#            # LOG THIS EVENT SOMEHOW
#            # Good enough for now...
#            log.warning("Is {} a FASTQ file?".format(input_file))
#            # sys.exit(1)

        except Exception as e:
            log.error(e)
            exit(1)
    log.info("Analysis finished.")


if __name__ == "__main__":
    main()
